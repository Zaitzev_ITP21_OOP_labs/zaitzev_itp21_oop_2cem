﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2
{
    public class Cloth
    {
        private string id;
        private string type;
        private double length;
        private double width;
        private string producer;
        private double price;
        public string Id { get { return id; } set { id = value; } }
        public string Type { get { return type; } set { type = value; } }
        public double Length
        {
            get { return length; }
            set
            {
                if(value <= 0) { throw new Exception("Ошибка ввода.Длина должна быть больше 0"); }
                else { length = value; }
            }
        }
        public double Width
        {
            get { return width; }
            set
            {
                if (value <= 0) { throw new Exception("Ошибка ввода.Ширина должна быть больше 0"); }
                else { width = value; }
            }
        }
        public double Price
        {
            get { return price; }
            set
            {
                if (value <= 0) { throw new Exception("Ошибка ввода.Цена должна быть больше 0"); }
                else { price = value; }
            }
        }
        public string Producer { get { return producer; } set { producer = value; } }
        /// <summary>
        /// Конструктор класса рулон ткани
        /// </summary>
        /// <param name="cypher">шифр ткани</param>
        /// <param name="type">тип ткани</param>
        /// <param name="length">длина ткани в рулоне</param>
        /// <param name="width">ширина рулона</param>
        /// <param name="producer">производитель ткани</param>
        public Cloth(string id, string type, double width, double length, string producer, double price)
        {
            Id = id;
            Type = type;
            if (length <= 0 || width <= 0 || price<=0)
            { throw new Exception("Ошибка ввода.Длина и ширина должны быть больше 0"); }
            else
            {
                Length = length;
                Width = width;
                Price = price;
            }
            Producer = producer;
        }
        public Cloth() { }
        public static int CompareByPrice(Cloth cloth1, Cloth cloth2)
        {
            return cloth1.Price.CompareTo(cloth2.Price);
        }
        public override string ToString()
        {
            return "| Id: " + Id +
             "| Producer: " + Producer +
             "| Type: " + Type +
             "| Width: " + Width.ToString() +
             "| Length: " + Length.ToString() +
             "| Price: " + Price.ToString(); 
        }
    }
    public class WidthComparer : IComparer<Cloth>
    {
        public int Compare(Cloth c1, Cloth c2)
        {
            if (c1.Width > c2.Width) return 1;
            else if (c1.Width == c2.Width) return 0;
            else return 0;
        }
    }
    public class LengthComparer : IComparer<Cloth>
    {
        public int Compare(Cloth c1, Cloth c2)
        {
            if (c1.Length > c2.Length) return 1;
            else if (c1.Length == c2.Length) return 0;
            else return 0;
        }
    }
    public class PriceComparer : IComparer<Cloth>
    {
        public int Compare(Cloth c1, Cloth c2)
        {
            if (c1.Price > c2.Price) return 1;
            else if (c1.Price == c2.Price) return 0;
            else return 0;
        }
    }

}

