﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Lab6Library;
using lab2;
using System.IO;
using System.Windows;

namespace Lab6App
{
    public partial class Form1 : Form
    {
        string path = @"D:\Studying\4cem\OOP\Lab6\input.txt";
        private GenericList<Cloth> clothes;
        public Form1()
        {
            InitializeComponent();
            deleteButton.Visible = false;
            sortButton.Visible = false;
        }

        private void UpdateList()
        {
            listBox1.Items.Clear();
            foreach (Cloth cloth in clothes)
                listBox1.Items.Add(cloth);
        }
        private void button1_Click(object sender, EventArgs e)
        {
            clothes = new GenericList<Cloth>();
            try
            {
                using (FileStream fs = new FileStream(path, FileMode.Open))
                {
                    byte[] array = new byte[fs.Length];
                    fs.Read(array, 0, array.Length);
                    string text = System.Text.Encoding.Default.GetString(array);

                    string[] textArr = text.Split('\n');
                    string[] strArr;
                    for (int i = 0; i < textArr.Length; i++)
                    {
                        strArr = textArr[i].Split(' ', '|'); // Можно добавить разделители;
                        clothes.Add(new Cloth
                        {
                            Id = strArr[0],
                            Producer = strArr[1],
                            Type = strArr[2],
                            Length = double.Parse(strArr[3]),
                            Width = double.Parse(strArr[4]),
                            Price =  double.Parse(strArr[5])
                        });
                    }
                    if (textArr.Length == clothes.Count)
                    {
                        MessageBox.Show("Чтение из файла завершено успешно.");
                        UpdateList();
                        deleteButton.Visible = true;
                        sortButton.Visible = true;
                    }
                }
            }
            catch
            {
                MessageBox.Show("Ошибка при считывании с файла");
            }
        }

        private void sortButton_Click(object sender, EventArgs e)
        {
            clothes.Sort(Cloth.CompareByPrice);
            UpdateList();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                clothes.Remove(clothes[listBox1.SelectedIndex]);
                UpdateList();
            }
            catch
            {
                MessageBox.Show("Ошибка при удалении записи.");
            }
        }


    }
}
