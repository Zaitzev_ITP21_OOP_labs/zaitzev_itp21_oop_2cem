﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6Library
{
    public class GenericList<T> : ICollection<T>
    {
        private T[] arrayT;
        public T this[int index] { get { return arrayT[index]; } set { arrayT[index] = value; } }
        public GenericList()
        {
            arrayT = new T[0];
        }

        public GenericList(int n)
        {
            arrayT = new T[n];
        }

        public GenericList(IEnumerable<T> enumerable)
        {
            arrayT = enumerable.ToArray();
        }

        public int Count => arrayT.Length;

        public bool IsReadOnly => false;

        public void Add(T item)
        {
            T[] buffer = new T[arrayT.Length + 1];
            for (int i = 0; i < arrayT.Length; i++)
                buffer[i] = arrayT[i];
            buffer[buffer.Length - 1] = item;
            arrayT = buffer;
        }

        public void Clear()
        {
            arrayT = new T[0];
        }

        public bool Contains(T item)
        {
            foreach (T t in arrayT)
            {
                if (t.Equals(item))
                    return true;
            }
            return false;
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            for (int i = 0; i < arrayT.Length; i++)
                array[i] = arrayT[i];
        }

        public IEnumerator<T> GetEnumerator()
        {
            for (int i = 0; i < arrayT.Length; i++)
            {
                yield return arrayT[i];
            }
        }

        public bool Remove(T item)
        {
            if (arrayT.Contains(item))
            {
                T[] newArr = new T[this.Count() - 1];
                int flag = 0;
                for (int i = 0; i < arrayT.Length; i++)
                {
                    if (arrayT[i].Equals(item))
                    {
                        flag = 1;
                        continue;
                    }
                    newArr[i - flag] = arrayT[i];
                }
                arrayT = newArr;
                return true;
            }
            return false;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            for (int i = 0; i < arrayT.Length; i++)
            {
                yield return arrayT[i];
            }
        }

        public void Sort(Comparison<T> comparison)
        {
            for (int i = 0; i < Count; i++)
            {
                for (int j = Count - 1; j > i; j--)
                {
                    if (comparison(arrayT[j - 1], arrayT[j]) == 1)
                    {
                        T temp = arrayT[j - 1];
                        arrayT[j - 1] = arrayT[j];
                        arrayT[j] = temp;
                    }
                }
            }
        }
    }
}
