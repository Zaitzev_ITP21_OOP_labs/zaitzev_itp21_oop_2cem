﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using System.Xml.Schema;

namespace lab2
{
    public class ClothDOMParser : IDisposable
    {
        /// <summary>
        /// класс предназначенный для работы с DOM-деревом в с#
        /// </summary>
        XmlDocument document;

        /// <summary>
        /// класс для добавления xsd-схемы к нашему xml-документу
        /// </summary>
        XmlReaderSettings settings;

        /// <summary>
        /// просто вспомогательный класс для создания класса XmlDocument
        /// </summary>
        XmlReader reader;
        /// <summary>
        /// Первый конструктор, для создания парсера когда мы передаем путь по которому лежит xml-документ
        /// </summary>
        /// <param name="xmlFilePath"></param>
        public ClothDOMParser(string xmlFilePath)
        {
            //загружаем xml-схему
            SetXmlSettings();

            //создаем ридер для создания XmlDocument
            reader = XmlReader.Create(xmlFilePath, settings);

            //в этом методе создаем сам XmlDocument (олицетворение DOM-дерева в c#)
            SetXmlDocument(reader);
        }
        /// <summary>
        /// Второй конструктор, для создания парсера когда мы сразу передаем текст xml-документа
        /// </summary>
        /// <param name="byteArray"></param>
        public ClothDOMParser(byte[] byteArray)
        {
            SetXmlSettings();
            reader = XmlReader.Create(new MemoryStream(byteArray), settings);
            SetXmlDocument(reader);
        }
        private void SetXmlSettings()
        {
            XmlTextReader reader = new XmlTextReader("D:\\Studying\\4cem\\OOP\\lab2\\lab2\\bin\\Debug\\Clothes.xsd");
            //XmlTextReader reader = new XmlTextReader("Clothes.xsd");
            XmlSchema schema = XmlSchema.Read(reader, ValidationCallback);
            settings = new XmlReaderSettings();
            settings.Schemas.Add(schema);
            settings.ValidationType = ValidationType.Schema;
        }

        private void SetXmlDocument(XmlReader reader)
        {
            document = new XmlDocument();
            document.Load(reader);
        }
        /// <summary>
        /// Обработчик события, на случай если xml-документ не соотвествует xsd-схеме
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        static void ValidationCallback(object sender, ValidationEventArgs args)
        {
            if (args.Severity == XmlSeverityType.Warning)
                Console.Write("WARNING: ");
            else if (args.Severity == XmlSeverityType.Error)
                Console.Write("ERROR: ");

            Console.WriteLine(args.Message);
        }
        /// <summary>
        /// Считываем xml-документ с помощью XPath
        /// </summary>
        /// <returns></returns>
        public List<Cloth> GetClothes()
        {
            List<Cloth> clothes = new List<Cloth>();
            string id = "", type = "", producer = "";
            double width = 0.1, length = 0.1, price = 0.1;
            XmlElement root = document.DocumentElement;
            XmlNodeList clothNodes = root.SelectNodes("Cloth");
            foreach (XmlNode clothNode in clothNodes)
            {
                try { id = clothNode.SelectSingleNode("Id").InnerText; } catch { throw new Exception("Ошибка считывания Id"); }
                try { type = clothNode.SelectSingleNode("Type").InnerText; } catch { throw new Exception("Ошибка считывания Type"); }
                try { length = Convert.ToDouble(clothNode.SelectSingleNode("Length").InnerText); if (length <= 0) { throw new Exception(); } } catch { throw new Exception("Ошибка считывания Length"); }
                try { width = Convert.ToDouble(clothNode.SelectSingleNode("Width").InnerText); if (width <= 0) { throw new Exception(); } } catch { throw new Exception("Ошибка считывания Width"); }
                try { producer = clothNode.SelectSingleNode("Producer").InnerText; } catch { throw new Exception("Ошибка считывания Producer"); }
                try { price = Convert.ToDouble(clothNode.SelectSingleNode("Price").InnerText); if (price <= 0) { throw new Exception(); } } catch { throw new Exception("Ошибка считывания Price"); }
                try { clothes.Add(new Cloth(id, type, width, length, producer, price)); } catch { throw new Exception("Ошибка считывания создания объекта Cloth"); }                
            }
            return clothes;
        }
        public void WriteInFile(string filePath, string idText, string typeText, string widthText, string lengthText, string producerText, string priceText)
        {
            XmlElement cloth = document.CreateElement("Cloth");
            XmlElement id = document.CreateElement("Id");
            XmlElement type = document.CreateElement("Type");
            XmlElement width = document.CreateElement("Width");
            XmlElement length = document.CreateElement("Length");
            XmlElement producer = document.CreateElement("Producer");
            XmlElement price = document.CreateElement("Price");

            id.AppendChild(document.CreateTextNode(idText));
            type.AppendChild(document.CreateTextNode(typeText));
            width.AppendChild(document.CreateTextNode(widthText));
            length.AppendChild(document.CreateTextNode(lengthText));
            producer.AppendChild(document.CreateTextNode(producerText));
            price.AppendChild(document.CreateTextNode(priceText));

            cloth.AppendChild(id);
            cloth.AppendChild(type);
            cloth.AppendChild(length);
            cloth.AppendChild(width);
            cloth.AppendChild(producer);
            cloth.AppendChild(price);

            XmlElement root = document.DocumentElement;
            root.AppendChild(cloth);
            cloth.RemoveAllAttributes();
            Dispose();
            document.Save(filePath);
        }
        public void WriteInFile(List<Cloth> clothList, string filePath)
        {
            XmlElement root = document.DocumentElement;
            XmlNodeList clothNodes = root.SelectNodes("Cloth");
            root = document.DocumentElement;
            root.RemoveAll();
            foreach (Cloth elem in clothList)
            {
                XmlElement cloth = document.CreateElement("Cloth");
                XmlElement id = document.CreateElement("Id");
                XmlElement type = document.CreateElement("Type");
                XmlElement width = document.CreateElement("Width");
                XmlElement length = document.CreateElement("Length");
                XmlElement producer = document.CreateElement("Producer");
                XmlElement price = document.CreateElement("Price");

                id.AppendChild(document.CreateTextNode(elem.Id));
                type.AppendChild(document.CreateTextNode(elem.Type));
                width.AppendChild(document.CreateTextNode(elem.Width.ToString()));
                length.AppendChild(document.CreateTextNode(elem.Length.ToString()));
                producer.AppendChild(document.CreateTextNode(elem.Producer));
                price.AppendChild(document.CreateTextNode(elem.Price.ToString()));

                cloth.AppendChild(id);
                cloth.AppendChild(type);
                cloth.AppendChild(length);
                cloth.AppendChild(width);
                cloth.AppendChild(producer);
                cloth.AppendChild(price);

                root = document.DocumentElement;
                root.AppendChild(cloth);
                cloth.RemoveAllAttributes();
                Dispose();
                document.Save(filePath);
            }

            
        }
        /// <summary>
        /// Реализуем метод Dispose т.к. этот класс работает с потоками
        /// </summary>
        public void Dispose()
        {
            DisposeManagedResources();
        }

        /// <summary>
        /// Освобождаем управляемые ресурсы (закрываем поток)
        /// </summary>
        protected virtual void DisposeManagedResources()
        {
            reader.Dispose();
        }
    }
    //объеденить по шифру типу ткани,отображать различные рулоны
}
