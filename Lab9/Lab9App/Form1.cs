﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace Lab9App
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dataGridView1.AutoGenerateColumns = true;

            if (new OpenFileDialog() is var dialog && dialog.ShowDialog() == DialogResult.OK) dataGridView1.DataSource = File.ReadLines(dialog.FileName).Where(str => str.Split(' ').ToList().Count == 4).Select(l => l.Split(' ')).ToList().Select(l => new User { Id = Int32.Parse(l[0]), Username = l[1], Password = l[2], Age = Int32.Parse(l[3]) }).ToList().Where(user => (user.Age >=18 && user.Age <=33)).OrderBy(user => user.Username).ToList();

        }
    }
}
