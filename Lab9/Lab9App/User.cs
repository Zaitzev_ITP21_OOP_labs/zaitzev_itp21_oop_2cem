﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab9App
{
    class User
    {
        private int age;
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int Age { get { return age; } set { if(value>0) { age = value; } } }
    }
}
