﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DatabaseClassLibrary;
using DatabaseClassLibrary.Objects;

namespace Lab7App
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string connectionString = ConnectionString.connectionString;
        public ProducerSQL producerSQL;
        public ClothTypeSQL clothTypeSQL;
        public ClothSQL clothSQL;

        Producer producer;
        ClothType clothType;
        Cloth cloth;
        public MainWindow()
        {
            InitializeComponent();

            producerSQL = new ProducerSQL(connectionString);
            producerSQL.Read();
            clothTypeSQL = new ClothTypeSQL(connectionString);
            clothTypeSQL.Read();
            clothSQL = new ClothSQL(connectionString);
            clothSQL.Read();

            AddingObjects_Init();

            GridProducers.ItemsSource = producerSQL.Producers;

            GridClothTypes.ItemsSource = clothTypeSQL.ClothTypes;

            GridCloth.ItemsSource = clothSQL.Clothes;

            ProducerCB.ItemsSource = producerSQL.Producers;

            ClothTypeCB.ItemsSource = clothTypeSQL.ClothTypes;


        }
        private void AddingObjects_Init()
        {
            producer = new Producer { ProducerName = "default" };
            clothType = new ClothType { TypeName = "default" };
            cloth = new Cloth { Cost = 1, Length = 1, Width = 1, ProducerID = producerSQL[0].ProducerID, TypeID = clothTypeSQL[0].TypeID };
        }

        private void ButtonUpdate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                producerSQL.Update();
                clothTypeSQL.Update();
                clothSQL.Update();
                MessageBox.Show("Обновление упешно завершено.");
            }
            catch
            {
                MessageBox.Show("Ошибка при обновлении записей.");
            }
            finally
            {
                UpdateTables();
            }
        }

        private void AddProducer_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                producerSQL.Producers.Add(producer);
                producerSQL.Insert(producer.ProducerName);
            }
            catch
            {
                MessageBox.Show("Ошибка при добавлении записи.");
            }
            finally
            {
                UpdateTables();
            }
        }
        private void UpdateTables()
        {
            producerSQL.Read();
            clothTypeSQL.Read();
            clothSQL.Read();

            GridProducers.ItemsSource = producerSQL.Producers;
            GridClothTypes.ItemsSource = clothTypeSQL.ClothTypes;
            GridCloth.ItemsSource = clothSQL.Clothes;

            ProducerCB.ItemsSource = producerSQL.Producers;
            ClothTypeCB.ItemsSource = clothTypeSQL.ClothTypes;
        }

        private void AddClothType_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                clothTypeSQL.ClothTypes.Add(clothType);
                clothTypeSQL.Insert(clothType.TypeName);
            }
            catch
            {
                MessageBox.Show("Ошибка при добавлении записи.");
            }
            finally
            {
                UpdateTables();
            }
        }

        private void RemoveClothType_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                clothTypeSQL.Delete(GridClothTypes.SelectedIndex);
            }
            catch
            {
                MessageBox.Show("Ошибка при удалении записи.");
            }
            finally
            {
                UpdateTables();
            }
        }

        private void RemoveProducer_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                producerSQL.Delete(GridProducers.SelectedIndex);
            }
            catch
            {
                MessageBox.Show("Ошибка при удалении записи.");
            }
            finally
            {
                UpdateTables();
            }
        }

        private void RemoveCloth_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                clothSQL.Delete(GridCloth.SelectedIndex);
            }
            catch
            {
                MessageBox.Show("Ошибка при удалении записи.");
            }
            finally
            {
                UpdateTables();
            }
        }

        private void AddCloth_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                clothSQL.Clothes.Add(cloth);
                clothSQL.Insert(cloth.TypeID, cloth.ProducerID, cloth.Width, cloth.Cost, cloth.Length);
            }
            catch
            {
                MessageBox.Show("Ошибка при добавлении записи.");
            }
            finally
            {
                UpdateTables();
            }
        }
    }
}
