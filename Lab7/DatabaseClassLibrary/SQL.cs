﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseClassLibrary
{
    public abstract class SQL
    {
        protected string connectionString;
        public SQL(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public abstract void Read();
        public abstract void Update();
        public abstract void Delete(int index);
    }
}
