﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseClassLibrary.Objects
{
    public class ClothType
    {
        public int TypeID { get; set; }
        public string TypeName { get; set; }
    }
}
