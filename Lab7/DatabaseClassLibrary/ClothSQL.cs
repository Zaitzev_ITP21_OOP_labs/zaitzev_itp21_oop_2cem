﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using DatabaseClassLibrary.Objects;

namespace DatabaseClassLibrary
{
    public class ClothSQL : SQL
    {
        public List<Objects.Cloth> Clothes { get; private set; }
        public Objects.Cloth this[int index] { get { return Clothes[index]; } }
        public ClothSQL(string connectionString) : base(connectionString) { }
        public override void Delete(int index)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                string sql = $"delete from Clothes where ClothID = @deleteID";
                SqlCommand command = new SqlCommand(sql, sqlConnection);
                command.Parameters.AddWithValue("@deleteID", Clothes[index].ClothID);
                int number = command.ExecuteNonQuery();
                if (number == 0 || number == -1)
                    throw new Exception();
            }
        }
        public override void Read()
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                string sql = @"select * from Clothes";
                SqlCommand command = new SqlCommand(sql, connection);
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    Clothes = new List<Objects.Cloth>();
                    while (reader.Read())
                    {
                        var temp = new Cloth();
                        temp.ClothID = reader.GetInt32(0);
                        temp.TypeID = reader.GetInt32(1);
                        temp.ProducerID = reader.GetInt32(2);
                        temp.Length = float.Parse(reader.GetDouble(3).ToString());
                        temp.Width = float.Parse(reader.GetDouble(4).ToString());
                        temp.Cost = float.Parse(reader.GetDouble(5).ToString());

                        Clothes.Add(temp);
                    }
                }
            }
        }
        public override void Update()
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                string sql;
                SqlCommand command;
                foreach (Objects.Cloth cloth in Clothes)
                {
                    sql = "update Clothes set" +
                        " TypeID = @typeID," +
                        " ProducerID = @producerID," +
                        " Length = @length," +
                        " Width = @width ," +
                        " Cost = @cost" +
                        " where ClothID = @clothID";
                    command = new SqlCommand(sql, connection);
                    command.Parameters.AddWithValue("@typeID", cloth.TypeID);
                    command.Parameters.AddWithValue("@producerID", cloth.ProducerID);
                    command.Parameters.AddWithValue("@length", cloth.Length);
                    command.Parameters.AddWithValue("@width", cloth.Width);
                    command.Parameters.AddWithValue("@cost", cloth.Cost);
                    command.Parameters.AddWithValue("@clothID", cloth.ClothID);
                    int number = command.ExecuteNonQuery();
                    if (number == 0 || number == -1)
                        throw new Exception();
                }
            }
        }
        public void Insert(int TypeID, int ProducerID, float Width, float Cost, float Length)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                string sql = "insert Clothes(TypeID, ProducerID, Length, Width, Cost) values(@typeID, @producerID, @length,@width, @cost)";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@typeID", TypeID);
                command.Parameters.AddWithValue("@producerID", ProducerID);
                command.Parameters.AddWithValue("@length", Length);
                command.Parameters.AddWithValue("@width", Width);
                command.Parameters.AddWithValue("@cost", Cost);
                int number = command.ExecuteNonQuery();
                if (number == 0)
                    throw new Exception();
            }
        }
    }
}
