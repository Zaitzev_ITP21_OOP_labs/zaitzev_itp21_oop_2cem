﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace DatabaseClassLibrary
{
    public class ProducerSQL : SQL
    {
        public List<Objects.Producer> Producers { get; private set; }
        public Objects.Producer this[int index] { get { return Producers[index]; } }
        public ProducerSQL(string connectionString) : base(connectionString) { }
        public override void Delete(int index)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                string sql = $"delete from Producers where ProducerID = @deleteID";
                SqlCommand command = new SqlCommand(sql, sqlConnection);
                command.Parameters.AddWithValue("@deleteID", Producers[index].ProducerID);
                int number = command.ExecuteNonQuery();
                if (number == 0)
                    throw new Exception();
            }
        }
        public override void Read()
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                string sql = @"select * from Producers";
                SqlCommand command = new SqlCommand(sql, connection);
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    Producers = new List<Objects.Producer>();
                    while (reader.Read())
                    {
                        Producers.Add(new Objects.Producer
                        {
                            ProducerID = reader.GetInt32(0),
                            ProducerName = reader.GetString(1)
                        });
                    }
                }
            }
        }
        public override void Update()
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                string sql;
                SqlCommand command;
                foreach (Objects.Producer cloth in Producers)
                {
                    sql = "update Producers set" +
                        " ProducerName = @producerName " +
                        " where ProducerID = @producerID";
                    command = new SqlCommand(sql, connection);
                    command.Parameters.AddWithValue("@producerID", cloth.ProducerID);
                    command.Parameters.AddWithValue("@producerName", cloth.ProducerName);
                    int number = command.ExecuteNonQuery();
                    if (number == 0 || number == -1)
                        throw new Exception();
                }
            }
        }
        public void Insert(string ProducerName)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                string sql = "insert Producers(ProducerName) values(@producerName)";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@producerName", ProducerName);
                int number = command.ExecuteNonQuery();
                if (number == 0)
                    throw new Exception();
            }
        }
    }
}
