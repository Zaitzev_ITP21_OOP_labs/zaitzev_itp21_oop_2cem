﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace DatabaseClassLibrary
{
    public class ClothTypeSQL : SQL
    {
        public List<Objects.ClothType> ClothTypes { get; private set; }
        public Objects.ClothType this[int index] { get { return ClothTypes[index]; } }
        public ClothTypeSQL(string connectionString) : base(connectionString) { }
        public override void Delete(int index)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                string sql = $"delete from ClothTypes where TypeID = @deleteID";
                SqlCommand command = new SqlCommand(sql, sqlConnection);
                command.Parameters.AddWithValue("@deleteID", ClothTypes[index].TypeID);
                int number = command.ExecuteNonQuery();
                if (number == 0 || number == -1)
                    throw new Exception();
            }
        }
        public override void Read()
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                string sql = @"select * from ClothTypes";
                SqlCommand command = new SqlCommand(sql, connection);
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    ClothTypes = new List<Objects.ClothType>();
                    while (reader.Read())
                    {
                        ClothTypes.Add(new Objects.ClothType
                        {
                            TypeID = reader.GetInt32(0),
                            TypeName = reader.GetString(1)
                        });
                    }
                }
            }
        }
        public override void Update()
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                string sql;
                SqlCommand command;
                foreach (Objects.ClothType cloth in ClothTypes)
                {
                    sql = "update ClothTypes set" +
                        " TypeName = @typeName " +
                        " where TypeID = @typeID";
                    command = new SqlCommand(sql, connection);
                    command.Parameters.AddWithValue("@typeID", cloth.TypeID);
                    command.Parameters.AddWithValue("@typeName", cloth.TypeName);
                    int number = command.ExecuteNonQuery();
                    if (number == 0 || number == -1)
                        throw new Exception();
                }
            }
        }
        public void Insert(string TypeName)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                string sql = "insert ClothTypes(TypeName) values(@typeName)";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@typeName", TypeName);
                int number = command.ExecuteNonQuery();
                if (number == 0)
                    throw new Exception();
            }
        }
    }
}
