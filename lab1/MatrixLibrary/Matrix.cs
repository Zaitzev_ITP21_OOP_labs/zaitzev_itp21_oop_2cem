﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace MatrixLibrary
{
    public class Matrix
    {
        private double[,] matrix;
        public Matrix(double[,] matrix)
        {
            //создаём матрицу с размерностью массива из которого клонируем
            this.matrix = new double[matrix.GetLength(0), matrix.GetLength(1)];
            try
            {
                int i, j;
                for (i = 0; i < matrix.GetLength(0); i++)
                {
                    for (j = 0; j < matrix.GetLength(1); j++)
                    {
                        if (Object.ReferenceEquals(null, matrix[i, j]) == true)
                        { throw new Exception("Один или несколько элементов матрицы равны null"); }
                    }
                }
                Array.Copy(matrix, 0, this.matrix, 0, matrix.Length);
            }
            catch
            { this.matrix = new double[1, 1]; this.matrix[0, 0] = 0; }
        }
        //генератор перестановок
        private static bool NextPermutation(int[] a)
        {
            if (a.Length == 0) return false;
            for (var n = a.Length - 1; ;)
            {
                var n1 = n;
                if (a[--n] < a[n1])
                {
                    var m = a.Length;
                    while (a[n] >= a[--m]) ;
                    var t = a[n]; a[n] = a[m]; a[m] = t;
                    Array.Reverse(a, n1, a.Length - n1);
                    return true;
                }
                if (n == 0)
                {
                    Array.Reverse(a);
                    return false;
                }
            }
        }
        //метод для подсчёта факториала
        //private static int Factorial(int number)
        public static int Factorial(int number)
        {
            if(Object.ReferenceEquals(null, number) == true || number<0)
            { throw new Exception("Не число или меньше 0"); }
            int k = 0;
            int result = 1;
            for (k = 1; k <= number; k++)
            { result = result * k; }
            return result;
        }
        //метод для подсчёта инверсий
        //private static int Reserve(int[] arr)
        private static int Reserve(int[] arr)
        {
            int i, j, n, result;
            for (i = 0; i < arr.Length; i++)
            {
                //throw new Exception("Один или несколько элементов массива равны null");
                if(Object.Equals(null, arr[i]) == true)
                { throw new Exception("Один или несколько элементов массива равны null"); }
            }
            n = 0;
            for (i = 0; i < arr.Length; i++)
            {
                for (j = i + 1; j < arr.Length; j++)
                {
                    if (arr[i] > arr[j])
                    { n = n + 1; }
                }
            }
            result = Convert.ToInt32(Math.Pow(-1, n));
            //Console.WriteLine("степень n "+n+"\nрезультат "+result);
            return result;
        }
        //метод для вычисления определителя
        public double Determinant()
        {
            double result = 0;
            bool flag = true;
            int i, j, k;
            for (i = 0; i < matrix.GetLength(0); i++)
            {
                for (j = 0; j < matrix.GetLength(1); j++)
                {
                    if (Object.ReferenceEquals(null, matrix[i, j]) == true)
                    { flag = false; }
                }
            }
            if (matrix.GetLength(0) != matrix.GetLength(1))
            { throw new Exception("Определить можно найти только для квадратной матрицы"); }
            //else if(Object.ReferenceEquals(null, this))
            //{ new Exception("Матрица равна null"); }
            else if (flag == false)
            { throw new Exception("Один или несколько элементов матрицы равны null"); }
            else
            {
                //int[] permutations = new int[Factorial(matrix.GetLength(0))];
                //массив для хранения перестановок
                List<int[]> permutations = new List<int[]>();
                //генерируем перестановки
                int[] a = new int[matrix.GetLength(0)];
                for (i = 0; i < matrix.GetLength(0); i++)
                { a[i] = i; }
                //int[] b = { 1, 2, 3, 4, 5 };
                do
                {

                    //foreach (var e in a) Console.Write(" " + e);
                    //Console.WriteLine();
                    string str = "";
                    foreach (var elem in a)
                    { str += Convert.ToString(elem); str += ";"; }

                    Regex splitter = new Regex(@"\;");
                    string[] strNums = splitter.Split(str);
                    int[] temp = new int[matrix.GetLength(0)];
                    for (i = 0; i < matrix.GetLength(0); i++)
                    {
                        temp[i] = Convert.ToInt32(strNums[i]);
                    }
                    permutations.Add(temp);

                } while (NextPermutation(a));
                //Console.WriteLine(permutations.Count);
                result = 0;
                //суммируем
                for (i = 0; i < Factorial(matrix.GetLength(0)); i++)
                {
                    double temp = 1;
                    for (k = 0; k < matrix.GetLength(0); k++)
                    { temp = temp * matrix[k, permutations[i][k]]; }
                    temp = temp * Reserve(permutations[i]);
                    result += temp;
                    //Console.WriteLine(temp);
                }
            }
            return result;
        }
        public double GetElement(int i, int j)
        {
            if(i<0 || i> matrix.GetLength(0) || j < 0 || j > matrix.GetLength(1))
            { throw new Exception("Такого элемента не существует"); }
            return matrix[i, j];
        }
        public string OutputMatrix()
        {
            string result = "";
            int i, j;
            for (i = 0; i < matrix.GetLength(0); i++)
            {
                result += "|";
                for (j = 0; j < matrix.GetLength(1); j++)
                { result = result + matrix[i, j].ToString("f5") + "|"; }
                result += "\n";
            }
            return result;
        }
        public override bool Equals(object obj)
        {
            bool flag = true;
            if (obj.GetType() != this.GetType()) { flag = false; }
            Matrix temp = (Matrix)obj;
            int i, j;
            if(this.matrix.GetLength(0) == temp.matrix.GetLength(0) && this.matrix.GetLength(1) == temp.matrix.GetLength(1))
            {
                int n = this.matrix.GetLength(0);
                int m = temp.matrix.GetLength(1);
                for (i = 0; i < n && flag == true; i++)
                {
                    for (j = 0; j < m && flag == true; j++)
                    {
                        if(this.matrix[i,j] != temp.matrix[i, j]) { flag = false; }
                    }
                }
            }
            else
            { flag = false; }
            return flag;
        }
    }
    /*
    //Реализация Enumerator для класса Matrix
    public class Matrices : IEnumerable
    {
        private Matrix[] _matrices;
        public Matrices(Matrix[] mArray)
        {
            _matrices = new Matrix[mArray.Length];
            for (int i = 0; i < mArray.Length; i++)
            {
                _matrices[i] = mArray[i];
            }
        }
        
        IEnumerator IEnumerable.GetEnumerator()
        {
            return (IEnumerator)GetEnumerator();
        }
        public MatricesEnum GetEnumerator()
        {
            return new MatricesEnum(_matrices);
        }
        public string OutputMatrix(int pos)
        {
            return _matrices[pos].OutputMatrix();
        }
    }
    public class MatricesEnum : IEnumerator
    {
        public Matrix[] _matrices;

        // Enumerators are positioned before the first element
        // until the first MoveNext() call.
        int position = -1;

        public MatricesEnum(Matrix[] list)
        {
            _matrices = list;
        }

        public bool MoveNext()
        {
            position++;
            return (position < _matrices.Length);
        }

        public void Reset()
        {
            position = -1;
        }

        object IEnumerator.Current
        {
            get
            {
                return Current;
            }
        }

        public Matrix Current
        {
            get
            {
                try
                {
                    return _matrices[position];
                }
                catch (IndexOutOfRangeException)
                {
                    throw new InvalidOperationException();
                }
            }
        }
    }
    */
}
