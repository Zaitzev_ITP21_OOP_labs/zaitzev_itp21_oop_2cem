﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MatrixLibrary;

namespace MatrixUnitTestProject
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            double[,] tempArr = new double[2, 2];
            tempArr[0, 0] = 2;
            tempArr[0, 1] = 3;
            tempArr[1, 0] = 4;
            tempArr[1, 1] = 5;
            Matrix matrix = new Matrix(tempArr);
            double temp = -2;
            Assert.AreEqual(temp, matrix.Determinant());
        }
        [TestMethod]
        public void TestMethod2()
        {
            double[,] tempArr = new double[2, 3];
            tempArr[0, 0] = 2;
            tempArr[0, 1] = 3;
            tempArr[0, 2] = 3;
            tempArr[1, 0] = 4;
            tempArr[1, 1] = 5;
            tempArr[1, 2] = 3;
            Matrix matrix = new Matrix(tempArr);
            Assert.ThrowsException<Exception>(() => { matrix.Determinant(); }, "Определить можно найти только для квадратной матрицы");
        }
        [TestMethod]
        public void TestMethod3()
        {
            int number = -2;
            Assert.ThrowsException<Exception>(() => { Matrix.Factorial(number); }, "Не число или меньше 0");
        }
        [TestMethod]
        public void TestMethod4()
        {
            double[,] tempArr = new double[2, 2];
            tempArr[0, 0] = 2;
            tempArr[0, 1] = 3;
            tempArr[1, 0] = 4;
            tempArr[1, 1] = 5;
            Matrix matrix = new Matrix(tempArr);
            Assert.ThrowsException<IndexOutOfRangeException>(() => { matrix.GetElement(2, 2); }, "");
        }
        [TestMethod]
        public void TestMethod5()
        {
            double[,] tempArr = new double[2, 2];
            tempArr[0, 0] = 2;
            tempArr[0, 1] = 3;
            tempArr[1, 0] = 4;
            Matrix matrix = new Matrix(tempArr);
            Assert.AreEqual(0, matrix.GetElement(1,1));
        }
        [TestMethod]
        public void TestMethod6()
        {
            double[,] tempArr = new double[2, 2];
            tempArr[0, 0] = 2;
            tempArr[0, 1] = 3;
            tempArr[1, 0] = 4;
            tempArr[1, 1] = 5;
            Matrix matrix1 = new Matrix(tempArr);
            tempArr[0, 0] = 2;
            tempArr[0, 1] = 3;
            tempArr[1, 0] = 4;
            tempArr[1, 1] = 4;
            Matrix matrix2 = new Matrix(tempArr);
            Assert.AreNotEqual(matrix1, matrix2);
        }
    }
}
