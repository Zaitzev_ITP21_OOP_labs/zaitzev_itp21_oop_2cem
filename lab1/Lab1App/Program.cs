﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MatrixLibrary;

namespace Lab1App
{
    class Program
    {
        

        static void Main(string[] args)
        {
            int menu = 1;
            double[,] arr = new double[1, 1];
            arr[0, 0] = 0;
            Matrix matrix = new Matrix(arr);
            while(menu!=4)
            {
                Console.WriteLine("Меню:");
                Console.WriteLine("1. Ввести матрицу");
                Console.WriteLine("2. Посчитать определитель для введённой матрицы");
                Console.WriteLine("3. Вывести матрицу");
                Console.WriteLine("4. Выход");
                Console.WriteLine("Введите пункт меню: ");
                try
                {
                    menu = Convert.ToInt32(Console.ReadLine());
                    switch(menu)
                    {
                        case 1:
                            {
                                int i, j, n, m;
                                try
                                {
                                    Console.WriteLine("Введите размерность матрицы");
                                    Console.WriteLine("Введите n: ");
                                    n = Convert.ToInt32(Console.ReadLine());
                                    Console.WriteLine("Введите m: ");
                                    m = Convert.ToInt32(Console.ReadLine());
                                    if(m<0 || n<0)
                                    { new Exception(); }
                                    else
                                    {
                                        double[,] temp = new double[n, m];
                                        for(i=0;i<n;i++)
                                        {
                                            for(j=0;j<m;j++)
                                            {
                                                Console.WriteLine("Введите элемент [" + (i + 1) + "," + (j + 1) + "]: ");
                                                temp[i, j] = Convert.ToDouble(Console.ReadLine());
                                            }
                                        }
                                        matrix = new Matrix(temp);
                                    }
                                }
                                catch
                                { Console.WriteLine("Число введено некорректно"); }
                            }break;
                        case 2:
                            {
                                try { Console.WriteLine("Определитель матрицы равен " + matrix.Determinant()); }
                                catch { Console.WriteLine("Определитель можно посчитать только для квадратных матриц"); }
                            }break;
                        case 3:
                            {
                                Console.WriteLine(matrix.OutputMatrix());
                            }
                            break;
                        case 4: break;
                        default:
                        { Console.WriteLine("Такого пункта меню не существует"); } break;
                    }
                }
                catch
                { Console.WriteLine("Ошибка ввода пункта меню"); }
            }
            Console.ReadKey();
        }
    }
}
