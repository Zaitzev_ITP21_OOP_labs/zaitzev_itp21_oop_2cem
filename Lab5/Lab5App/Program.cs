﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab5Lib;

namespace Lab5App
{
    class Program
    {
        static void Main(string[] args)
        {
            Integral integral = new Integral();
            int a = 15;
            int b = 18;
            Console.WriteLine("a_1 = " + integral.LowerBorder.ToString());
            Console.WriteLine("b_1 = " + integral.UpperBorder.ToString());

            integral.LowerBorder = b;
            integral.UpperBorder = a;
            Console.WriteLine("a_1 = " + integral.LowerBorder.ToString());
            Console.WriteLine("b_1 = " + integral.UpperBorder.ToString());

            integral.UpperBorder = 21;
            Console.WriteLine("a_1 = " + integral.LowerBorder.ToString());
            Console.WriteLine("b_1 = " + integral.UpperBorder.ToString());


            integral.LowerBorder = 10;
            Console.WriteLine("a_1 = " + integral.LowerBorder.ToString());
            Console.WriteLine("b_1 = " + integral.UpperBorder.ToString());



            Console.ReadKey();
        }
    }
}
