﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lab5Lib;

namespace Lab5UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestSwitch()
        {
            Integral integral = new Integral();
            int a = 15;
            int b = 17;
            integral.LowerBorder = b;
            integral.UpperBorder = a;
            Assert.AreEqual(a, integral.LowerBorder);
            Assert.AreEqual(b, integral.UpperBorder);
        }
        [TestMethod]
        public void TestIntegral()
        {
            Integral integral = new Integral();
            int a = 15;
            int b = 17;
            integral.LowerBorder = b;
            integral.UpperBorder = a;

            double result = 0.0279581;

            Function function = CustomFunc;

            double temp = Math.Abs(result - integral.CalcIntegral(function, 10000));

            Assert.IsTrue(temp <= 0.001);
        }
        public double CustomFunc(double x) { return Math.Sin(x) / x; }
    }
}
