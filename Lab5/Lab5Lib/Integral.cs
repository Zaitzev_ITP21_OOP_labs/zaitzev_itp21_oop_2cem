﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5Lib
{
    public delegate double Function(double x);
    public class Integral
    {
        private double a;
        private double b;
        private delegate void SwitchBordersDelegate();
        private event SwitchBordersDelegate customEvent;
        private void SwitchBorders()
        {
            double temp = 0;
            temp = a;
            a = b;
            b = temp;
        }
        public double UpperBorder
        {
            get
            {
                if (b < a) { customEvent?.Invoke(); }
                return b; 
            }
            set
            {
                b = value;
            }
        }
        public double LowerBorder
        {
            get
            {
                if (a > b) { customEvent?.Invoke(); }
                return a;
            }
            set
            {
                a = value;

            }
        }
        //метод левых прямоугольников
        /// <summary>
        /// Вычисление численного значения интеграла с помощью
        /// метода левых прямоугольников
        /// </summary>
        /// <param name="func">Вычисление значения функции в заданной точке</param>
        /// <param name="n">количество шагов</param>
        /// <returns></returns>
        public double CalcIntegral(Function func, int n)
        {
            double result = 0;
            double h = (b - a) / n;
            int i;
            double tempX = a;
            for (i = 0; i < n; i++)
            {
                result += func(tempX);
                tempX += h;
            }
            result *= h;
            return result;
        }
        /// <summary>
        /// Ссылка на метод для вычисления значения функции в точке  
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        

        public Integral()
        {
            customEvent += SwitchBorders;
            a = 0;
            b = 1;
            
        }

    }
}
