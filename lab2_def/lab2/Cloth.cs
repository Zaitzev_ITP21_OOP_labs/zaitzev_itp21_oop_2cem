﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2
{
    public class Cloth
    {
        public class Roll
        {
            private double length;
            private double width;
            private double price;
            public double Length
            {
                get { return length; }
                set
                {
                    if (value <= 0) { throw new Exception("Ошибка ввода.Длина должна быть больше 0"); }
                    else { length = value; }
                }
            }
            public double Width
            {
                get { return width; }
                set
                {
                    if (value <= 0) { throw new Exception("Ошибка ввода.Ширина должна быть больше 0"); }
                    else { width = value; }
                }
            }
            public double Price
            {
                get { return price; }
                set
                {
                    if (value <= 0) { throw new Exception("Ошибка ввода.Цена должна быть больше 0"); }
                    else { price = value; }
                }
            }
            public Roll(double width, double length, double price)
            {
                if (length <= 0 || width <= 0 || price <= 0)
                { throw new Exception("Ошибка ввода.Длина и ширина должны быть больше 0"); }
                else
                {
                    Length = length;
                    Width = width;
                    Price = price;
                }
            }
        }
        public List<Roll> rolls;
        private int id;
        private string type;
        private string producer;

        public int Id { get { return id; } set { id = value; } }
        public string Type { get { return type; } set { type = value; } }
        public string Producer { get { return producer; } set { producer = value; } }
        /// <summary>
        /// Конструктор класса рулон ткани
        /// </summary>
        /// <param name="cypher">шифр ткани</param>
        /// <param name="type">тип ткани</param>
        /// <param name="length">длина ткани в рулоне</param>
        /// <param name="width">ширина рулона</param>
        /// <param name="producer">производитель ткани</param>
        public Cloth(int id, string type, string producer)
        {
            rolls = new List<Roll>();
            Id = id;
            Type = type;
            Producer = producer;
        }
        public void AddRoll(double width, double length, double price)
        {
            if (length <= 0 || width <= 0 || price <= 0)
            { throw new Exception("Ошибка ввода.Длина и ширина должны быть больше 0"); }
            else
            { rolls.Add(new Roll(width, length, price)); }
            
        }
        public static void AddObject(List<Cloth> clothes, int id, string type, string producer, double width, double length, double price)
        {
            bool isOld = false;
            int oldId = 0;
            int i;
            for(i=0;i<clothes.Count;i++)
            {
                if (id == clothes[i].Id && type == clothes[i].Type && producer == clothes[i].Producer) { isOld = true; oldId = i; break; }
            }
            if (isOld == false)
            {
                Cloth temp = new Cloth(id, type, producer);
                temp.AddRoll(width, length, price);
                clothes.Add(temp);
            }
            else { clothes[i].AddRoll(width, length, price); } 
        }
        public Cloth() { rolls = new List<Roll>(); }
        public static List<string> GetClothes(List<Cloth> clothes)
        {
            List<string> result = new List<string>();
            foreach (Cloth cloth in clothes)
            {
                foreach (Roll roll in cloth.rolls)
                {
                    result.Add("Id: "+cloth.Id.ToString()+" Тип ткани: "+cloth.Type+ " Производитель: " + cloth.Producer + " Длина: " + roll.Length.ToString() + " Ширина: " + roll.Width.ToString() + " Цена: " + roll.Price.ToString());
                }
            }
            return result;
        }
    }
    /*
    public class WidthComparer : IComparer<Cloth>
    {
        public int Compare(Cloth c1, Cloth c2)
        {
            if (c1.Width > c2.Width) return 1;
            else if (c1.Width == c2.Width) return 0;
            else return 0;
        }
    }
    public class LengthComparer : IComparer<Cloth>
    {
        public int Compare(Cloth c1, Cloth c2)
        {
            if (c1.Length > c2.Length) return 1;
            else if (c1.Length == c2.Length) return 0;
            else return 0;
        }
    }
    public class PriceComparer : IComparer<Cloth>
    {
        public int Compare(Cloth c1, Cloth c2)
        {
            if (c1.Price > c2.Price) return 1;
            else if (c1.Price == c2.Price) return 0;
            else return 0;
        }
    }
    */
}
