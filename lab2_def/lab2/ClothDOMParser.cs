﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using System.Xml.Schema;

namespace lab2
{
    public class ClothDOMParser : IDisposable
    {
        /// <summary>
        /// класс предназначенный для работы с DOM-деревом в с#
        /// </summary>
        XmlDocument document;

        /// <summary>
        /// класс для добавления xsd-схемы к нашему xml-документу
        /// </summary>
        XmlReaderSettings settings;

        /// <summary>
        /// просто вспомогательный класс для создания класса XmlDocument
        /// </summary>
        XmlReader reader;
        /// <summary>
        /// Первый конструктор, для создания парсера когда мы передаем путь по которому лежит xml-документ
        /// </summary>
        /// <param name="xmlFilePath"></param>
        public ClothDOMParser(string xmlFilePath)
        {
            //загружаем xml-схему
            SetXmlSettings();

            //создаем ридер для создания XmlDocument
            reader = XmlReader.Create(xmlFilePath, settings);

            //в этом методе создаем сам XmlDocument (олицетворение DOM-дерева в c#)
            SetXmlDocument(reader);
        }
        /// <summary>
        /// Второй конструктор, для создания парсера когда мы сразу передаем текст xml-документа
        /// </summary>
        /// <param name="byteArray"></param>
        public ClothDOMParser(byte[] byteArray)
        {
            SetXmlSettings();
            reader = XmlReader.Create(new MemoryStream(byteArray), settings);
            SetXmlDocument(reader);
        }
        private void SetXmlSettings()
        {
            XmlTextReader reader = new XmlTextReader("D:\\Studying\\4cem\\OOP\\lab2\\lab2\\bin\\Debug\\Clothes.xsd");
            //XmlTextReader reader = new XmlTextReader("Clothes.xsd");
            XmlSchema schema = XmlSchema.Read(reader, ValidationCallback);
            settings = new XmlReaderSettings();
            settings.Schemas.Add(schema);
            settings.ValidationType = ValidationType.Schema;
        }

        private void SetXmlDocument(XmlReader reader)
        {
            document = new XmlDocument();
            document.Load(reader);
        }
        /// <summary>
        /// Обработчик события, на случай если xml-документ не соотвествует xsd-схеме
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        static void ValidationCallback(object sender, ValidationEventArgs args)
        {
            if (args.Severity == XmlSeverityType.Warning)
                Console.Write("WARNING: ");
            else if (args.Severity == XmlSeverityType.Error)
                Console.Write("ERROR: ");

            Console.WriteLine(args.Message);
        }
        /// <summary>
        /// Считываем xml-документ с помощью XPath
        /// </summary>
        /// <returns></returns>
        public List<Cloth> GetClothes()
        {
            List<Cloth> clothes = new List<Cloth>();
            int id = 0;
            string type = "", producer = "";
            double width = 0.1, length = 0.1, price = 0.1;
            XmlElement root = document.DocumentElement;
            XmlNodeList clothNodes = root.SelectNodes("Cloth");
            foreach (XmlNode clothNode in clothNodes)
            {
                try { id = Convert.ToInt32(clothNode.SelectSingleNode("Id").InnerText); } catch { throw new Exception("Ошибка считывания Id"); }
                try { type = clothNode.SelectSingleNode("Type").InnerText; } catch { throw new Exception("Ошибка считывания Type"); }
                try { producer = clothNode.SelectSingleNode("Producer").InnerText; } catch { throw new Exception("Ошибка считывания Producer"); }
                Cloth temp = new Cloth();
                try { temp = new Cloth(id, type, producer);  } catch { throw new Exception("Ошибка считывания создания объекта Cloth"); }
                XmlNodeList rollNodes = clothNode.SelectNodes("Roll");
                foreach (XmlNode rollNode in rollNodes)
                {
                    try { length = Convert.ToDouble(rollNode.SelectSingleNode("Length").InnerText); if (length <= 0) { throw new Exception(); } } catch { throw new Exception("Ошибка считывания Length"); }
                    try { width = Convert.ToDouble(rollNode.SelectSingleNode("Width").InnerText); if (width <= 0) { throw new Exception(); } } catch { throw new Exception("Ошибка считывания Width"); }
                    try { price = Convert.ToDouble(rollNode.SelectSingleNode("Price").InnerText); if (price <= 0) { throw new Exception(); } } catch { throw new Exception("Ошибка считывания Price"); }
                    temp.AddRoll(width,length,price);
                }
                clothes.Add(temp);
            }
            return clothes;
        }
        public void WriteInFile(List<Cloth> clothList, string filePath)
        {
            XmlElement root = document.DocumentElement;
            XmlNodeList clothNodes = root.SelectNodes("Cloth");
            root = document.DocumentElement;
            root.RemoveAll();
            foreach (Cloth elem in clothList)
            {
                XmlElement cloth = document.CreateElement("Cloth");
                XmlElement id = document.CreateElement("Id");
                XmlElement type = document.CreateElement("Type");
                XmlElement producer = document.CreateElement("Producer");

                id.AppendChild(document.CreateTextNode(elem.Id.ToString()));
                type.AppendChild(document.CreateTextNode(elem.Type));
                producer.AppendChild(document.CreateTextNode(elem.Producer));

                cloth.AppendChild(id);
                cloth.AppendChild(type);
                cloth.AppendChild(producer);

                foreach (Cloth.Roll roll in elem.rolls)
                {
                    XmlElement singleRoll = document.CreateElement("Roll");

                    XmlElement length = document.CreateElement("Length");
                    XmlElement width = document.CreateElement("Width");
                    XmlElement price = document.CreateElement("Price");
                    length.AppendChild(document.CreateTextNode(roll.Length.ToString()));
                    width.AppendChild(document.CreateTextNode(roll.Width.ToString()));
                    price.AppendChild(document.CreateTextNode(roll.Price.ToString()));
                    singleRoll.AppendChild(length);
                    singleRoll.AppendChild(width);
                    singleRoll.AppendChild(price);
                    cloth.AppendChild(singleRoll);
                }
                root = document.DocumentElement;
                root.AppendChild(cloth);
                cloth.RemoveAllAttributes();
                Dispose();
                document.Save(filePath);
            }
            

        }
        /// <summary>
        /// Реализуем метод Dispose т.к. этот класс работает с потоками
        /// </summary>
        public void Dispose()
        {
            DisposeManagedResources();
        }

        /// <summary>
        /// Освобождаем управляемые ресурсы (закрываем поток)
        /// </summary>
        protected virtual void DisposeManagedResources()
        {
            reader.Dispose();
        }
    }
    //объеденить по шифру типу ткани,отображать различные рулоны
}
