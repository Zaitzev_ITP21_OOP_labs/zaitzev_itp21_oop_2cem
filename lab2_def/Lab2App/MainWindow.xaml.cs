﻿using System;
using Microsoft.Win32;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using lab2;

namespace Lab2App
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public List<Cloth> clothList;
        public List<Cloth> table;
        ClothDOMParser parser;
        public MainWindow()
        {
            InitializeComponent();
            clothList = new List<Cloth>();
            table = new List<Cloth>();
            UpdateBox();
        }
        public void UpdateBox()
        {
            table.Clear();
            table = clothList;
            ClothBox.ItemsSource = Cloth.GetClothes(table);
            
        }

        void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //Создаем парсер передавая сам xml-текст из textBox (тем, самым выполняя проверку на валидность. Если он не валидный, то выскочет исключение и парсер не будет создан)
                //var checkIfValid = new ClothDOMParser(Encoding.UTF8.GetBytes(textBox.Text));

                SaveFileDialog dlg = new SaveFileDialog();
                dlg.Filter = "XML Files (*.xml)|*.xml";

                if (dlg.ShowDialog() == true)
                {
                    clothList.Clear();
                    parser.WriteInFile(table, dlg.FileName);
                    //освобождаем захваченные ресурсы
                    parser?.Dispose();

                    MessageBox.Show("Файл сохранён");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// Диалоговое окно для открытия xml-документа
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OpenButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog dlg = new OpenFileDialog();
                dlg.Filter = "XML Files (*.xml)|*.xml";

                if (dlg.ShowDialog() == true)
                {
                    //создали парсер указав путь к файлу (используется первый конструктор в ClothDOMParser)
                    //MessageBox.Show(dlg.FileName);
                    parser = new ClothDOMParser(dlg.FileName);
                    //вывели путь файла в textBox
                    textBox.Text = dlg.FileName;
                    clothList.Clear();
                    try
                    {
                        clothList = parser.GetClothes();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        //MessageBox.Show("Ошибка считывания данных");
                    }
                    UpdateBox();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


    }
}
