﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;
using System.IO;
using Lab4Lib;

namespace Lab4App
{
    public partial class Form1 : Form
    {
        List<string> pictures;
        int rotateIndexRight;
        int rotateIndexLeft;
        int picIndex;
        Class1 class1;
        public Form1()
        {
            InitializeComponent();
            pictures = new List<string>();
            picIndex = 0;
            rotateIndexRight = 0;
            rotateIndexLeft = 0;
            class1 = new Class1();
            class1.CreateGUI(this);
            
        }

        private void prevPicButton_Click(object sender, EventArgs e)
        {
            if (pictures.Count >= 2)
            {
                try
                {
                    if (picIndex == 0)
                    { picIndex = pictures.Count - 1; }
                    else { picIndex--; }
                    pictureBox1.Image = Image.FromFile(pictures[picIndex]);
                    pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
                    rotateIndexRight = 1;
                    rotateIndexLeft = 1;
                }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
                
            }
            else { MessageBox.Show("Загружена только одна картинка"); }
        }
        private void rotateLeftButton_Click(object sender, EventArgs e)
        {
            if (pictures.Count != 0)
            {
                var picture = pictureBox1.Image;
                switch (rotateIndexLeft)
                {
                    case 1: picture.RotateFlip(RotateFlipType.Rotate90FlipXY); break;
                    case 2: picture.RotateFlip(RotateFlipType.Rotate90FlipXY); break;
                    case 3: picture.RotateFlip(RotateFlipType.Rotate90FlipXY); break;
                    case 4: picture.RotateFlip(RotateFlipType.Rotate90FlipXY); break;
                }
                if (rotateIndexLeft == 4) { rotateIndexLeft = 1; }
                rotateIndexLeft++;
                pictureBox1.Image = picture;
            }
            else { MessageBox.Show("Отсутствует изображение"); }
        }
        private void openPicturesButton_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    string[] files = Directory.GetFiles(fbd.SelectedPath);
                    pictures = new List<string>();
                    string picFolder = fbd.SelectedPath;
                    var temp1 = Directory.EnumerateFiles(picFolder, "*.jpg");
                    var temp2 = Directory.EnumerateFiles(picFolder, "*.bmp");
                    var temp3 = Directory.EnumerateFiles(picFolder, "*.gif");
                    foreach (string elem in temp1)
                    { pictures.Add(elem); }
                    foreach (string elem in temp2)
                    { pictures.Add(elem); }
                    foreach (string elem in temp3)
                    { pictures.Add(elem); }
                    MessageBox.Show("Files found: " + pictures.Count.ToString(), "Message");
                    if (pictures.Count >= 1)
                    {
                        pictureBox1.Image = Image.FromFile(pictures[0]); picIndex = 0;
                        pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
                        rotateIndexRight = 1;
                        rotateIndexLeft = 1;
                    }
                    
                }
            }
        }
        private void rotateRightButton_Click(object sender, EventArgs e)
        {
            if (pictures.Count != 0)
            {
                var picture = pictureBox1.Image;
                switch (rotateIndexRight)
                {
                    case 1: picture.RotateFlip(RotateFlipType.Rotate270FlipXY); break;
                    case 2: picture.RotateFlip(RotateFlipType.Rotate270FlipXY); break;
                    case 3: picture.RotateFlip(RotateFlipType.Rotate270FlipXY); break;
                    case 4: picture.RotateFlip(RotateFlipType.Rotate270FlipXY); break;
                }
                if (rotateIndexRight == 4) { rotateIndexRight = 1; }
                rotateIndexRight++;
                pictureBox1.Image = picture;
            }
            else { MessageBox.Show("Отсутствует изображение"); }
        }
        private void nextPicButton_Click(object sender, EventArgs e)
        {
            if (pictures.Count >= 2)
            {
                try
                {
                    if (picIndex == pictures.Count - 1)
                    { picIndex = 0; }
                    else { picIndex++; }
                    pictureBox1.Image = Image.FromFile(pictures[picIndex]);
                    pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
                    rotateIndexRight = 1;
                    rotateIndexLeft = 1;
                }
                catch(Exception ex) { MessageBox.Show(ex.Message); }
            }
            else { MessageBox.Show("Загружена только одна картинка"); }
        }
    }
}
