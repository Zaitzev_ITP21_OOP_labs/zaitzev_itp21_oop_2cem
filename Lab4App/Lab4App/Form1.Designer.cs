﻿namespace Lab4App
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.prevPicButton = new System.Windows.Forms.Button();
            this.rotateLeftButton = new System.Windows.Forms.Button();
            this.rotateRightButton = new System.Windows.Forms.Button();
            this.nextPicButton = new System.Windows.Forms.Button();
            this.openPicturesButton = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // prevPicButton
            // 
            this.prevPicButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.prevPicButton.Location = new System.Drawing.Point(10, 740);
            this.prevPicButton.Name = "prevPicButton";
            this.prevPicButton.Size = new System.Drawing.Size(160, 80);
            this.prevPicButton.TabIndex = 0;
            this.prevPicButton.Text = "Предыдущее\r\nизображение";
            this.prevPicButton.UseVisualStyleBackColor = true;
            this.prevPicButton.Visible = false;
            this.prevPicButton.Click += new System.EventHandler(this.prevPicButton_Click);
            // 
            // rotateLeftButton
            // 
            this.rotateLeftButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rotateLeftButton.Location = new System.Drawing.Point(180, 740);
            this.rotateLeftButton.Name = "rotateLeftButton";
            this.rotateLeftButton.Size = new System.Drawing.Size(180, 80);
            this.rotateLeftButton.TabIndex = 1;
            this.rotateLeftButton.Text = "Повернуть на\r\n90° влево ";
            this.rotateLeftButton.UseVisualStyleBackColor = true;
            this.rotateLeftButton.Visible = false;
            this.rotateLeftButton.Click += new System.EventHandler(this.rotateLeftButton_Click);
            // 
            // rotateRightButton
            // 
            this.rotateRightButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rotateRightButton.Location = new System.Drawing.Point(940, 740);
            this.rotateRightButton.Name = "rotateRightButton";
            this.rotateRightButton.Size = new System.Drawing.Size(180, 80);
            this.rotateRightButton.TabIndex = 2;
            this.rotateRightButton.Text = "Повернуть на\r\n90° вправо ";
            this.rotateRightButton.UseVisualStyleBackColor = true;
            this.rotateRightButton.Visible = false;
            this.rotateRightButton.Click += new System.EventHandler(this.rotateRightButton_Click);
            // 
            // nextPicButton
            // 
            this.nextPicButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.nextPicButton.Location = new System.Drawing.Point(1130, 740);
            this.nextPicButton.Name = "nextPicButton";
            this.nextPicButton.Size = new System.Drawing.Size(160, 80);
            this.nextPicButton.TabIndex = 3;
            this.nextPicButton.Text = "Следующее\r\nизображение";
            this.nextPicButton.UseVisualStyleBackColor = true;
            this.nextPicButton.Visible = false;
            this.nextPicButton.Click += new System.EventHandler(this.nextPicButton_Click);
            // 
            // openPicturesButton
            // 
            this.openPicturesButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.openPicturesButton.Location = new System.Drawing.Point(563, 740);
            this.openPicturesButton.Name = "openPicturesButton";
            this.openPicturesButton.Size = new System.Drawing.Size(200, 80);
            this.openPicturesButton.TabIndex = 4;
            this.openPicturesButton.Text = "Открыть папку \r\nс фотографиями";
            this.openPicturesButton.UseVisualStyleBackColor = true;
            this.openPicturesButton.Visible = false;
            this.openPicturesButton.Click += new System.EventHandler(this.openPicturesButton_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(10, 10);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1280, 720);
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1299, 831);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.openPicturesButton);
            this.Controls.Add(this.nextPicButton);
            this.Controls.Add(this.rotateRightButton);
            this.Controls.Add(this.rotateLeftButton);
            this.Controls.Add(this.prevPicButton);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button prevPicButton;
        private System.Windows.Forms.Button rotateLeftButton;
        private System.Windows.Forms.Button rotateRightButton;
        private System.Windows.Forms.Button nextPicButton;
        private System.Windows.Forms.Button openPicturesButton;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

