﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;
using System.IO;
using System.Drawing;

namespace Lab4Lib
{
    public class Class1
    {
        List<string> pictures;
        int rotateIndexRight;
        int rotateIndexLeft;
        int picIndex;
        public delegate void GenerateGUI(Form form);
        public List<string> Pictures
        {
            get { return pictures; }
            set
            {
                pictures = value;
                picIndex = 0;
                rotateIndexRight = 1;
                rotateIndexLeft = 1;
            }
        }
        public void CreateGUI(Form form)
        {
            form.Height = 870;
            form.Width = 1315;
            CreatePrevPicButton(form);
            CreateNextPicButton(form);
            CreateLeftRotateButton(form);
            CreateRightRotateButton(form);
            CreateOpenFolderButton(form);
            CreatePictureBox(form);
        }
        public void CreatePrevPicButton(Form form)
        {
            Button prevPicButton1 = new Button();
            prevPicButton1.Width = 160;
            prevPicButton1.Height = 80;
            prevPicButton1.Location = new Point(10, 740);
            prevPicButton1.Text = "Предыдущее изображение  ";
            prevPicButton1.Visible = true;
            form.Controls.Add(prevPicButton1);
            //rotateLeftButton1.Click += new EventHandler(rotateLeftButton1_Click)
        }
        public void CreateNextPicButton(Form form)
        {
            Button nextPicButton1 = new Button();
            nextPicButton1.Width = 160;
            nextPicButton1.Height = 80;
            nextPicButton1.Location = new Point(1130, 740);
            nextPicButton1.Text = "Следующее изображение  ";
            nextPicButton1.Visible = true;
            form.Controls.Add(nextPicButton1);
            //rotateLeftButton1.Click += new EventHandler(rotateLeftButton1_Click)
        }
        public void CreateLeftRotateButton(Form form)
        {
            Button rotateLeftButton1 = new Button();
            rotateLeftButton1.Width = 180;
            rotateLeftButton1.Height = 80;
            rotateLeftButton1.Location = new Point(180, 740);
            rotateLeftButton1.Text = "Повернуть на \n90° влево  ";
            rotateLeftButton1.Visible = true;
            form.Controls.Add(rotateLeftButton1);
            //rotateLeftButton1.Click += new EventHandler(rotateLeftButton1_Click)
        }
        public void CreateRightRotateButton(Form form)
        {
            Button rotateLeftButton2 = new Button();
            rotateLeftButton2.Width = 180;
            rotateLeftButton2.Height = 80;
            rotateLeftButton2.Location = new Point(940, 740);
            rotateLeftButton2.Text = "Повернуть на \n90° вправо  ";
            rotateLeftButton2.Visible = true;
            form.Controls.Add(rotateLeftButton2);
        }
        public void CreateOpenFolderButton(Form form)
        {
            Button openFolderButton1 = new Button();
            openFolderButton1.Width = 200;
            openFolderButton1.Height = 80;
            openFolderButton1.Location = new Point(563, 740);
            openFolderButton1.Text = " Открыть папку \nс фотографиями ";
            openFolderButton1.Visible = true;
            form.Controls.Add(openFolderButton1);
        }
        public void CreatePictureBox(Form form)
        {
            PictureBox pictureBox1 = new PictureBox();
            pictureBox1.Width = 200;
            pictureBox1.Height = 80;
            pictureBox1.Location = new Point(10, 10);
            pictureBox1.Visible = true;
            form.Controls.Add(pictureBox1);
        }
        public void OpenPictures(PictureBox pictureBox1)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    string[] files = Directory.GetFiles(fbd.SelectedPath);
                    pictures = new List<string>();
                    string picFolder = fbd.SelectedPath;
                    var temp1 = Directory.EnumerateFiles(picFolder, "*.jpg");
                    var temp2 = Directory.EnumerateFiles(picFolder, "*.bmp");
                    var temp3 = Directory.EnumerateFiles(picFolder, "*.gif");
                    foreach (string elem in temp1)
                    { pictures.Add(elem); }
                    foreach (string elem in temp2)
                    { pictures.Add(elem); }
                    foreach (string elem in temp3)
                    { pictures.Add(elem); }
                    MessageBox.Show("Files found: " + pictures.Count.ToString(), "Message");
                    if (pictures.Count >= 1)
                    {
                        pictureBox1.Image = Image.FromFile(pictures[0]); picIndex = 0;
                        pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
                        rotateIndexRight = 1;
                        rotateIndexLeft = 1;
                    }
                    
                }
            }
        }
    }
}
