﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;
using System.IO;
using Lab4;

namespace Lab4App
{
    public partial class Form1 : Form
    {
        public delegate void LoadPicture(string str);
        public event LoadPicture Notify;
        PictureExplorer pictureExplorer;
        PictureBox pictureBox;

        public Form1()
        {
            InitializeComponent();
            this.ClientSize = new System.Drawing.Size(1299, 831);
            pictureExplorer = new PictureExplorer();
            Notify += ShowMessage;
            pictureBox = AddPictureBox(10, 10, 720, 1280);
            pictureBox.Visible = true;
            this.Controls.Add(this.pictureBox);
            string text = "Предыдущее\r\nизображение";
            AddButton(text, 10, 740).Click += PicPrev;
            text = "Следующее\r\nизображение";
            AddButton(text, 1130, 740).Click += PicNext;
            text = "Открыть  \r\n папку";
            AddButton(text, 563, 740).Click += openPicturesButton_Click;
            text = "Повернуть на\r\n90° влево ";
            AddButton(text, 180, 740).Click += RotateLeft;
            text = "Повернуть на\r\n90° вправо ";
            AddButton(text, 940, 740).Click += RotateRight;

            //pictureBox.Image = Image.FromFile(@"D:\Studying\4cem\OOP\Lab4App\Pictures\rose.jpg");
            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox.Refresh();

        }
        private Button AddButton(string text, int x, int y)
        {
            return new Button
            {
                Text = text,
                TextAlign = ContentAlignment.MiddleCenter,
                Font = new Font("Times New Roman", 14, FontStyle.Regular),
                BackColor = Color.NavajoWhite,
                AutoSize = false,
                Width = 160,
                Height = 80,
                Parent = this,
                Location = new System.Drawing.Point(x, y),
                Visible = true
            };
        }
        private void SwitchPic()
        {
            try
            {
                pictureBox.Image = Image.FromFile(pictureExplorer.Picture);
                
                //pictureBox.Image = Image.FromFile(@"D:\Studying\4cem\OOP\Lab4App\Pictures\rose.jpg");
                pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                pictureBox.Refresh();

            }
            catch
            {
                Notify?.Invoke("Ошибка открытия изображения");
            }
        }
        private PictureBox AddPictureBox(int x, int y, int height, int width)
        {
            return new PictureBox
            {
                Name = "pictureBox",
                Location = new System.Drawing.Point(x, y),
                Size = new System.Drawing.Size(width, height),
                SizeMode = PictureBoxSizeMode.Zoom,
                Visible = true
            };
        }
        private static void ShowMessage(string str)
        {
            MessageBox.Show(str);
        }
        private void RotateRight(object sender, EventArgs e)
        {
            try
            {
                var picture = pictureBox.Image;
                pictureExplorer.RotateRight();
                switch (pictureExplorer.RotateRightIndex)
                {
                    case 1: picture.RotateFlip(RotateFlipType.Rotate270FlipXY); break;
                    case 2: picture.RotateFlip(RotateFlipType.Rotate270FlipXY); break;
                    case 3: picture.RotateFlip(RotateFlipType.Rotate270FlipXY); break;
                    case 4: picture.RotateFlip(RotateFlipType.Rotate270FlipXY); break;
                }
                pictureBox.Image = picture;
                pictureBox.Refresh();

            }
            catch
            {
                Notify?.Invoke("Ошибка поворота изображения");
            }
        }
        private void RotateLeft(object sender, EventArgs e)
        {
            try
            {
                var picture = pictureBox.Image;
                pictureExplorer.RotateLeft();
                switch (pictureExplorer.RotateLeftIndex)
                {
                    case 1: picture.RotateFlip(RotateFlipType.Rotate90FlipXY); break;
                    case 2: picture.RotateFlip(RotateFlipType.Rotate90FlipXY); break;
                    case 3: picture.RotateFlip(RotateFlipType.Rotate90FlipXY); break;
                    case 4: picture.RotateFlip(RotateFlipType.Rotate90FlipXY); break;
                }
                pictureBox.Image = picture;
                pictureBox.Refresh();

            }
            catch
            {
                Notify?.Invoke("Ошибка поворота изображения");
            }
        }
        private void PicNext(object sender, EventArgs e)
        {
            try
            {
                pictureExplorer.NextPic();
                SwitchPic();
            }
            catch
            {
                Notify?.Invoke("Ошибка переключения изображения");
            }
        }
        private void PicPrev(object sender, EventArgs e)
        {
            try
            {
                pictureExplorer.PrevPic();
                SwitchPic();
            }
            catch
            {
                Notify?.Invoke("Ошибка переключения изображения");
            }
        }
        private void openPicturesButton_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    string[] files = Directory.GetFiles(fbd.SelectedPath);
                    var pictures = new List<string>();
                    string picFolder = fbd.SelectedPath;
                    var temp1 = Directory.EnumerateFiles(picFolder, "*.jpg");
                    var temp2 = Directory.EnumerateFiles(picFolder, "*.bmp");
                    var temp3 = Directory.EnumerateFiles(picFolder, "*.gif");
                    foreach (string elem in temp1)
                    { pictures.Add(elem); }
                    foreach (string elem in temp2)
                    { pictures.Add(elem); }
                    foreach (string elem in temp3)
                    { pictures.Add(elem); }
                    MessageBox.Show("Files found: " + pictures.Count.ToString(), "Message");
                    if (pictures.Count >= 1)
                    {
                        pictureExplorer = new PictureExplorer(pictures);
                        SwitchPic();
                    }
                    else
                    {
                        Notify?.Invoke("В папке отсутствуют изображения");
                    }

                }
            }
        }
    }
}
