﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4
{
    public class PictureExplorer
    {
        List<Picture> pictures;
        int rotateIndexRight;
        int rotateIndexLeft;
        int picIndex;
        public delegate void RefreshPics();
        RefreshPics refreshPics;
        public List<string> Pictures
        {
            set
            {
                foreach(string str in value)
                {
                    pictures.Add(new Picture(str));
                }/*
                picIndex = 0;
                rotateIndexRight = 1;
                rotateIndexLeft = 1;*/
                refreshPics();
            }
        }
        public int RotateRightIndex { get { return rotateIndexRight; } }
        public int RotateLeftIndex { get { return rotateIndexLeft; } }
        public string Picture { get { return pictures[picIndex].ToString(); } }
        public PictureExplorer(List<string> pics)
        {
            refreshPics += () =>
            {
                picIndex = 0;
                rotateIndexRight = 1;
                rotateIndexLeft = 1;
            };
            pictures = new List<Picture>();
            this.Pictures = new List<string>();
            this.Pictures = pics;
        }
        public PictureExplorer()
        {
            refreshPics += () =>
            {
                picIndex = 0;
                rotateIndexRight = 1;
                rotateIndexLeft = 1;
            };
            pictures = new List<Picture>();
            this.Pictures = new List<string>();

        }
        public void PrevPic()
        {
            if (picIndex == 0)
            { picIndex = pictures.Count - 1; }
            else { picIndex--; }
            rotateIndexRight = 1;
            rotateIndexLeft = 1;
        }
        public void NextPic()
        {
            if (picIndex == pictures.Count - 1)
            { picIndex = 0; }
            else { picIndex++; }
            rotateIndexRight = 1;
            rotateIndexLeft = 1;
        }
        public void RotateRight()
        {
            if (rotateIndexLeft == 4) { rotateIndexLeft = 1; }
            rotateIndexLeft++;
        }
        public void RotateLeft()
        {
            if (rotateIndexRight == 4) { rotateIndexRight = 1; }
            rotateIndexRight++;
        }
    }
}
