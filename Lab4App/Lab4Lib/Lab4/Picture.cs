﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4
{
    class Picture
    {
        public string Pic { get; set; }
        public Picture(string str)
        {
            Pic = str;
        }
        public override string ToString()
        {
            return Pic;
        }
    }
}
