﻿using System;
using System.IO;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using lab3lib;

namespace lab3TestProject
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethodFileStream()
        {
            List<byte> byteList;
            int n = 12;
            byte[] buffer = new byte[n];

            Random random = new Random();
            random.NextBytes(buffer);
            using (FileStream fileStream = new FileStream(@"D:\Studying\4cem\OOP\lab3\test.txt",
                FileMode.Create))
            {
                using (ConcreteStreamDecorator concreteStream = new ConcreteStreamDecorator(fileStream))
                {
                    concreteStream.Write(buffer, 0, buffer.Length);
                    byteList = concreteStream.ByteList;
                }
            }
            CollectionAssert.AreEqual(buffer, byteList);
        }

        [TestMethod]
        public void TestMethodMemoryStream()
        {
            List<byte> byteList;
            int n = 16;
            byte[] buffer = new byte[n];

            Random random = new Random();
            random.NextBytes(buffer);

            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (ConcreteStreamDecorator concreteStream = new ConcreteStreamDecorator(memoryStream))
                {
                    concreteStream.Write(buffer, 0, buffer.Length);
                    byteList = concreteStream.ByteList;
                }
            }
            CollectionAssert.AreEqual(buffer, byteList);
        }

        [TestMethod]
        public void TestMethodBufferedStream()
        {
            List<byte> byteList;
            int n = 16;
            byte[] buffer = new byte[n];

            Random random = new Random();
            random.NextBytes(buffer);

            using (FileStream fileStream = new FileStream(@"D:\Studying\4cem\OOP\lab3\test.txt",
                FileMode.Open))
            {
                using (BufferedStream bufferedStream = new BufferedStream(fileStream))
                {
                    using (ConcreteStreamDecorator concreteStream = new ConcreteStreamDecorator(bufferedStream))
                    {
                        concreteStream.Write(buffer, 0, buffer.Length);
                        byteList = concreteStream.ByteList;
                    }
                }
            }
            CollectionAssert.AreEqual(buffer, byteList);
        }
    }
}
