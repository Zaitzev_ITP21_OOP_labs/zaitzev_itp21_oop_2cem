﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using lab3lib;

namespace lab3App
{
    public partial class Form1 : Form
    {
        List<byte> byteList = new List<byte>();
        byte[] array;
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            byteList.Clear();
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.Filter = "TXT файл(*.txt)|*.txt";

            if (openFile.ShowDialog() == DialogResult.OK)
            {
                using (FileStream fileStream = new FileStream(openFile.FileName, FileMode.Open))
                {
                    ConcreteStreamDecorator decorator = new ConcreteStreamDecorator(fileStream);
                    array = new byte[decorator.Length];
                    decorator.Read(array, 0, array.Length);
                    decorator.Close();
                    int n = 0;
                    try { n = Convert.ToInt32(textBox1.Text); }
                    catch { n = 0; }
                    byteList = decorator.GetNBytes(n);
                }

                UpdateListBox();
            }
        }
        private void UpdateListBox()
        {
            listBox1.Items.Clear();
            listBox2.Items.Clear();
            listBox1.Items.Add("Массив байтов:");
            for (int i = 0; i < array.Length; i++)
                listBox1.Items.Add(array[i].ToString());

            listBox2.Items.Add("Список байтов:");
            for (int i = 0; i < byteList.Count; i++)
                listBox2.Items.Add(byteList[i].ToString());
        }
    }
}
