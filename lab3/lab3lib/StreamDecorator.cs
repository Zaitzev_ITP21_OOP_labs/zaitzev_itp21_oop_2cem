﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace lab3lib
{
    public abstract class StreamDecorator : Stream
    {
        protected Stream stream;
        public StreamDecorator(Stream stream) : base()
        {
            this.stream = stream;
        }
    }
    public class ConcreteStreamDecorator : StreamDecorator
    {
        public List<byte> ByteList { get; private set; }

        public ConcreteStreamDecorator(Stream stream)
            : base(stream)
        { }

        public override bool CanRead => stream.CanRead;

        public override bool CanSeek => throw new NotImplementedException();

        public override bool CanWrite => stream.CanWrite;

        public override long Length => stream.Length;

        public override long Position { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public override void Flush()
        {
            throw new NotImplementedException();
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            int n = stream.Read(buffer, offset, count);
            ByteList = buffer.ToList();
            return n;
        }
        /// <summary>
        /// Получение последних n байт из сохранённого потока
        /// </summary>
        /// <param name="n">количество байт</param>
        /// <returns></returns>
        public List<byte> GetNBytes(int n)
        {
            int i;
            if(n> ByteList.Count) { n = 0; }
            List<byte> result = new List<byte>();
            for (i = ByteList.Count-n; i < ByteList.Count; i++)
            { result.Add(ByteList[i]); }
            return result;
        }
        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new NotImplementedException();
        }

        public override void SetLength(long value)
        {
            throw new NotImplementedException();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            stream.Write(buffer, offset, count);
            ByteList = buffer.ToList();
        }
    }
}
