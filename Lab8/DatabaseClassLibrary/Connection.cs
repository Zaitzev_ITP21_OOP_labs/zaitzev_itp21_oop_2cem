﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;

namespace DatabaseClassLibrary
{
    public class Connection
    {
        private string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        protected SqlConnection GetConnection()
        {
            return new SqlConnection(connectionString);
        }
    }
}
