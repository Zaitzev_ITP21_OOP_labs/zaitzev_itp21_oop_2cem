﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseClassLibrary.Objects
{
    public class Producer
    {
        public int ProducerID { get; set; }
        public string ProducerName { get; set; }
    }
}
