﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseClassLibrary.Objects
{
    public class Cloth
    {
        public int ClothID { get; set; }
        public float Length { get; set; }
        public float Width { get; set; }
        public float Cost { get; set; }
        public int TypeID { get; set; }
        public int ProducerID { get; set; }
    }
}
