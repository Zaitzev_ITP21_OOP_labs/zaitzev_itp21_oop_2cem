﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace DatabaseClassLibrary
{
    public class DataSetClass : Connection
    {
        public DataSet DataSet { get; private set; }
        string sqlProducers = "select * from Producers";
        string sqlClothTypes = "select * from ClothTypes";
        string sqlClothes = "select * from Clothes";
        public DataSetClass()
        {
            DataSet = new DataSet();
            Fill();
        }
        private void ReadProducers()
        {
            using (SqlConnection connection = GetConnection())
            {
                connection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(sqlProducers, connection);
                adapter.Fill(DataSet, "Producers");
            }
        }
        private void ReadClothTypes()
        {
            using (SqlConnection connection = GetConnection())
            {
                connection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(sqlClothTypes, connection);
                adapter.Fill(DataSet, "ClothTypes");
            }
        }
        private void ReadClothes()
        {
            using (SqlConnection connection = GetConnection())
            {
                connection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(sqlClothes, connection);
                adapter.Fill(DataSet, "Clothes");
            }
        }

        public void Fill()
        {
            ReadProducers();
            ReadClothTypes();
            ReadClothes();
        }
        public void Update()
        {
            using (SqlConnection connection = GetConnection())
            {
                connection.Open();

                SqlDataAdapter adapter = new SqlDataAdapter(sqlProducers, connection);
                SqlCommandBuilder commandBuilder = new SqlCommandBuilder(adapter);
                adapter.Update(DataSet, "Producers");

                adapter = new SqlDataAdapter(sqlClothTypes, connection);
                commandBuilder = new SqlCommandBuilder(adapter);
                adapter.Update(DataSet, "ClothTypes");

                adapter = new SqlDataAdapter(sqlClothes, connection);
                commandBuilder = new SqlCommandBuilder(adapter);
                adapter.Update(DataSet, "Clothes");

                DataSet.Clear();
                Fill();
            }
            
        }
        public void Delete(string tableName, int index)
        {
            DataSet.Tables[tableName].Rows[index].Delete();
            this.Update();
        }

    }
}
