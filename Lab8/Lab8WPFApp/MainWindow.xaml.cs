﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DatabaseClassLibrary;

namespace Lab8WPFApp
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DataSetClass dataSet;
        public MainWindow()
        {
            InitializeComponent();
            dataSet = new DataSetClass();

            GridProducers.SelectionMode = DataGridSelectionMode.Single;
            GridProducers.ItemsSource = dataSet.DataSet.Tables["Producers"].AsDataView();

            GridClothTypes.SelectionMode = DataGridSelectionMode.Single;
            GridClothTypes.ItemsSource = dataSet.DataSet.Tables["ClothTypes"].AsDataView();

            GridCloth.SelectionMode = DataGridSelectionMode.Single;
            GridCloth.ItemsSource = dataSet.DataSet.Tables["Clothes"].AsDataView();

            ProducerCB.ItemsSource = dataSet.DataSet.Tables["Producers"].DefaultView;
            //ProducerCB.DisplayMember = "ProducerName";
            //ProducerCB.ValueMember = "ProducerID";
            ClothTypeCB.ItemsSource = dataSet.DataSet.Tables["ClothTypes"].DefaultView;
            //TypeCB.DisplayMember = "TypeName";
            //TypeCB.ValueMember = "TypeID";
        }

        private void ButtonUpdate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                dataSet.Update();
                MessageBox.Show("БД успешно обновлена");
            }
            catch
            {
                MessageBox.Show("Ошибка обновления БД");
            }
            
        }
    }
}
